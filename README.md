> Auteur : Jordan Protin | jordan.protin@yahoo.com

# Demo HERE Technologies : API Geocoder

Il s'agit d'une petite démo permettant de tester l'API Geocoder by [HERE](https://developer.here.com/). Un article sur le sujet est disponible [ici](https://gitlab.com/jordanprotin/docs/-/wikis/HERE-:-API-Geocoder).

## Sommaire

1. [Prérequis](#prérequis)
2. [Installation](#installation)
3. [Troubleshooting](#warning-troubleshooting)

## Prérequis

- [GIT](https://installations.delicious-insights.com/software/git.html)
- [Docker](https://www.docker.com/products/docker-desktop)

## Installation

``` bash
$ git clone git@gitlab.com:jordanprotin/open/js/demo-here.git
```

:warning: Ensuite, n'oubliez pas de modifier les valeurs HERE **app_id and app_code** dans ces fichiers :

- `www/js/geocoding_position.js`
- `www/js/geocoding_suggestions.js`

## Lancement

Pour tester localement le projet, nous allons utiliser [docker](https://www.docker.com/) :

``` bash
$ cd demo-here
$ docker build -t demo_here .
$ docker run -p 80:80 demo_here
```

> :+1: Rendez-vous alors sur [http://localhost](http://localhost) pour accéder à cette demo.

## :warning: Troubleshooting

Vous pouvez obtenir une erreur au lancement : *Start fails with Error starting userland proxy: listen tcp 0.0.0.0:80: bind: address already in use*

> Assurez-vous alors de bien stopper tous les services (apache2, nginx) pouvant écouter sur le port 80.