
var GEOCODER_URL = 'https://geocoder.api.here.com/6.2/geocode.json',
    // initialize communication with the HERE platform
    APPLICATION_ID   = 'YOUR_HERE_APPLICATION_ID',
    APPLICATION_CODE = 'YOUR_HERE_APPLICATION_CODE';

/**
 * Geocoder request to retrieve the Location Details
 * 
 * @see https://developer.here.com/documentation/geocoder-autocomplete/topics/example-location-id.html
 */
function getPosition() {
	// locationId is provided by HERE
	var locationId = $(this).attr('data-location');

	$.ajax({
		url: GEOCODER_URL,
		type: 'GET',
		dataType: 'jsonp',
		jsonp: 'jsoncallback',
		data: {
			locationid: locationId,
			app_id: APPLICATION_ID,
			app_code: APPLICATION_CODE,
			jsonattributes: '1', // lowercase response keys
			gen: '9'
		},
		success: function (data) {
			var location  = data.response.view[0].result[0].location.displayPosition,
				latitude  = location.latitude,
				longitude = location.longitude;

			var displayText = 'Latitude : ' + latitude + ' / Longitude : ' + longitude;
			document.querySelector('#location').innerHTML = displayText;
		}
	});
}